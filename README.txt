Readme
------
This module lets users delete comments on nodes they create without giving them full comment administration access. Permissions are on a per node type basis, so it's a great way to, e.g., allow users to administer comments on their own blogs.

Additionally, you can configure this module to force comments on selected node types to be approved before they get published. As with delete rights, this is administered by users so you don't have to do it yourself.

Send comments to Gwen Park at: http://drupal.org/user/99925.

Requirements
------------
This module requires Drupal 5.x and the comment module.

Installation
------------
1. Copy the usercomment directory and its contents to the Drupal modules directory, probably /sites/all/modules/.

2. Enable the User Comment module in the modules admin page /admin/build/modules.

3. Enable the appropriate permissions on the user access admin page /admin/user/access.

4. Update the default approval email on the usercomment admin page /admin/content/comments/usercomment.

The "delete" functionality appears as a comment link courtesy of hook_link().

The approval form is available in the $node object for the node owner courtesy of hook_nodeapi(). You can add it to your node by modifying node.tpl.php in your theme. For example, if you wanted to add the code at the end of the node, you could add this to the end of node.tpl.php:
  <?php
    if (! empty($node->usercomment)) {
      print $node->usercomment;
    }
  ?>

Upgrading from a previous version
----------------------------------
1. Copy the usercomment directory and its contents to the Drupal modules directory, probably /sites/all/modules/.

2. Some of the user access option names have changed, so please set them appropriately at /admin/user/access.

Settings
--------
There are some user settings available on the user edit form.

Credits
-------
Written by Gwen Park.

TODO
----
1. Get rid of the hacks:
   * user access for hook_menu()
   * hard coded comment sql queries
